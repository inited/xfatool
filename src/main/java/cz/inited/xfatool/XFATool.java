package cz.inited.xfatool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDXFAResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XFATool {
    

    public static void main(String[] args) throws Exception {
        exportxfa("file.pdf", "export.xml");
        //importxfa("file.pdf", "export.xml", "novy.pdf");
    }
    
    public static void exportxfa(String indoc, String outxml) throws Exception {
        PDDocument doc = PDDocument.load(new FileInputStream(indoc));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();
        PDXFAResource xfa = form.getXFA();
        Document document = xfa.getDocument();
        
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        
        tr.transform(new DOMSource(document),  new StreamResult(new FileOutputStream(outxml)));
        
    }

    public static void importxfa(String indoc, String inxml, String outdoc) throws Exception {
        PDDocument doc = PDDocument.load(new FileInputStream(indoc));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        Document newDocument = documentBuilder.parse(new FileInputStream(inxml));

        
        COSStream cosout = doc.getDocument().createCOSStream();
        OutputStream out = cosout.createOutputStream();

        DOMSource source = new DOMSource(newDocument);
        StreamResult result = new StreamResult(out);
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.transform(source, result);
        
        PDXFAResource xfaout = new PDXFAResource(cosout);
        form.setXFA(xfaout);
        out.close();
        doc.save(outdoc);
    }


    
    
/*    
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        System.out.println("Ahoj");
        PDDocument doc = PDDocument.load(new FileInputStream("file.pdf"));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        //PDPage page = (PDPage)docCatalog.getAllPages().get(0);
        //page.setRotation(1);

        doc.save("novy.pdf");

    }

    public static void odstraneniAcroForm(String[] args) throws IOException, ParserConfigurationException, SAXException, COSVisitorException {
        System.out.println("Ahoj");
        PDDocument doc = PDDocument.load("file.pdf");
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();
        PDXFA xfa = form.getXFA();

        List<COSName> keys = docCatalog.getCOSDictionary().keyList();
        for(COSName key : keys) {
            System.out.println(key);
        }
        docCatalog.getCOSDictionary().removeItem(COSName.getPDFName("AcroForm"));

        doc.save("novy.pdf");

    }

*/    

    public static void xxmain(String[] args) throws Exception {
        PDDocument doc = PDDocument.load(new FileInputStream("file.pdf"));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        Document newDocument = documentBuilder.parse(new FileInputStream("novy.xml"));
        
        PDXFAResource xfa = form.getXFA();
        Document xfaDocument = xfa.getDocument();
        
        
        Element xfaDocumentElement = xfaDocument.getDocumentElement();
        NodeList els = xfaDocumentElement.getElementsByTagName("xfa:datasets");
        Element xfaDatasets = (Element) els.item(0);

        NodeList nlData = xfaDatasets.getElementsByTagName("xfa:data");
        Element elData = (Element) nlData.item(0);

        do {
            els = elData.getChildNodes();
            if (els.getLength() == 0) {
                break;
            }
            System.out.println("Smazat: " + els.item(0).getNodeName());
            elData.removeChild(els.item(0));
        } while(true);
        
        Node imported = xfaDocument.importNode(newDocument.getFirstChild(), true);
        
        elData.appendChild(imported);
        
        Transformer tr = TransformerFactory.newInstance().newTransformer();

        tr.transform(new DOMSource(xfaDocumentElement),  new StreamResult(new FileOutputStream("export.xml")));
        
        COSStream cosout = doc.getDocument().createCOSStream();
        OutputStream out = cosout.createOutputStream();

        DOMSource source = new DOMSource(xfaDocument);
        StreamResult result = new StreamResult(out);
        tr.transform(source, result);

        
        PDXFAResource xfaout = new PDXFAResource(cosout);
        form.setXFA(xfaout);
        out.close();
        doc.save("novy.pdf");

        //exportXFA(null);
    }

    
    
    
    public static void exportxfa2(String pdfFilename, String xmlFilename) throws Exception {
        PDDocument doc = PDDocument.load(new FileInputStream(pdfFilename));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

        PDXFAResource xfa = form.getXFA();
        Document document = xfa.getDocument();
        
        Element el = document.getDocumentElement();
        NodeList els = el.getElementsByTagName("xfa:datasets");
        el = (Element) els.item(0);

        els = el.getElementsByTagName("xfa:data");
        el = (Element) els.item(0);
        
        els = el.getChildNodes();
        el = (Element) els.item(0);
        
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        //tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        
        tr.transform(new DOMSource(el),  new StreamResult(new FileOutputStream("export.xml")));

    }
    
    /*
    public static void main(String[] args) throws Exception {
        System.out.println("Ahoj");
        PDDocument doc = PDDocument.load(new FileInputStream("file.pdf"));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        
        PDXFAResource xfa = form.getXFA();
        Document document = xfa.getDocument();
        
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        
        Element el2;
        
        Element el = document.getDocumentElement();
        NodeList els = el.getElementsByTagName("xfa:datasets");
        el = (Element) els.item(0);

        els = el.getElementsByTagName("xfa:data");
        el = (Element) els.item(0);
        
        els = el.getElementsByTagName("senat");
        el2 = (Element) els.item(0);
        
        el2.setTextContent("8888");
        
        System.out.println(el2.getTextContent());
        
        
        // send DOM to file
        //tr.transform(new DOMSource(el),  new StreamResult(new FileOutputStream("novy.xml")));

        COSStream cosout = doc.getDocument().createCOSStream();
        OutputStream out = cosout.createOutputStream();

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(out);
        tr.transform(source, result);

        
        PDXFAResource xfaout = new PDXFAResource(cosout);
        form.setXFA(xfaout);
        out.close();
        doc.save("novy.pdf");
    }
    
        
/*        
        
        COSBase cos = xfa.getCOSObject();
        COSStream coss = (COSStream) cos;
        InputStream cosin = coss.getUnfilteredStream();
        Document document = documentBuilder.parse(cosin);

        COSStream cosout = new COSStream();
        OutputStream out = cosout.createUnfilteredStream();

        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        
        DOMSource source = new DOMSource(xmlDoc);
        StreamResult result = new StreamResult(out);
        transformer.transform(source, result);

        PDXFAResource xfaout = new PDXFAResource(cosout);
        form.setXFA(xfaout);

        form.setXFA(xfa);
        

        //Document xfadoc = xfa.getDocument();
        byte[] xfabytes = xfa.getBytes();
        
        File xfaFile = new File("xfa.xfa");
        FileOutputStream fos = new FileOutputStream(xfaFile);
        fos.write(xfabytes);
        fos.close();

        doc.save("novy.pdf");

    }

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        System.out.println("Ahoj");
        PDDocument doc = PDDocument.load(new FileInputStream("file.pdf"));
        PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
        PDAcroForm form = docCatalog.getAcroForm();
        PDXFAResource xfa = form.getXFA();

        //Document xfadoc = xfa.getDocument();
        byte[] xfabytes = xfa.getBytes();
        
        File xfaFile = new File("xfa.xfa");
        FileOutputStream fos = new FileOutputStream(xfaFile);
        fos.write(xfabytes);
        fos.close();

    }
*/
}
